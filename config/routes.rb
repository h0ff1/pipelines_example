Rails.application.routes.draw do
  root to: 'hello#index'
  get '/hello/:name', to: 'hello#index', as: :hello
end
