class HelloController < ApplicationController
  def index
    @name = params[:name].capitalize if params[:name].present?
  end
end
